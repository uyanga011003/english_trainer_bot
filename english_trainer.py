import telebot
from telebot import types

bot = telebot.TeleBot('5593113635:AAEHgxCN-R2spf4dUwaTA7X8Qss0VFKxurk')

@bot.message_handler(commands=['start'])
def send_welcome(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard = True)
    key_begin = types.KeyboardButton('начать новый тест')
    markup.add(key_begin)
    bot.send_message(message.chat.id, "Привет! Я помогу тебе тренировать свой английский язык.", reply_markup = markup)

@bot.message_handler(content_types=['text'])
def get_text(message):
    if message.text == "начать новый тест":
        markup1 = types.InlineKeyboardMarkup()
        level1 = types.InlineKeyboardButton(text='Начальный', callback_data='beginner')
        markup1.add(level1)
        level2 = types.InlineKeyboardButton(text='Средний', callback_data='middle')
        markup1.add(level2)
        level3 = types.InlineKeyboardButton(text='Продвинутый', callback_data='pro')
        markup1.add(level3)
        bot.send_message(message.chat.id, "Выберите уровень: ", reply_markup=markup1)

@bot.callback_query_handler(func=lambda call: call.data in ["beginner",'middle','pro'])
def choose_section(call):
    if call.data=="beginner":
        markup = types.InlineKeyboardMarkup()
        section1 = types.InlineKeyboardButton(text='Грамматика', callback_data='grammar1')
        markup.add(section1)
        section2 = types.InlineKeyboardButton(text='Словообразование', callback_data='word_form1')
        markup.add(section2)
        section3 = types.InlineKeyboardButton(text='Новые слова', callback_data='vocabulary1')
        markup.add(section3)
        bot.send_message(call.message.chat.id, "Отлично! Теперь выберите раздел: ", reply_markup=markup)
    elif call.data=="middle":
        markup = types.InlineKeyboardMarkup()
        section1 = types.InlineKeyboardButton(text='Грамматика', callback_data='grammar2')
        markup.add(section1)
        section2 = types.InlineKeyboardButton(text='Словообразование', callback_data='word_form2')
        markup.add(section2)
        section3 = types.InlineKeyboardButton(text='Новые слова', callback_data='vocabulary2')
        markup.add(section3)
        bot.send_message(call.message.chat.id, "Отлично! Теперь выберите раздел: ", reply_markup=markup)
    else:
        markup = types.InlineKeyboardMarkup()
        section1 = types.InlineKeyboardButton(text='Грамматика', callback_data='grammar3')
        markup.add(section1)
        section2 = types.InlineKeyboardButton(text='Словообразование', callback_data='word_form3')
        markup.add(section2)
        section3 = types.InlineKeyboardButton(text='Новые слова', callback_data='vocabulary3')
        markup.add(section3)
        bot.send_message(call.message.chat.id, "Отлично! Теперь выберите раздел: ", reply_markup=markup)

@bot.callback_query_handler(func=lambda call: call.data in ["grammar1",'went','going','goes','is','are','am','could','will can','can'])
def test_bg(call):
    if call.data=="grammar1":
        q1="1.What is the correct verb form? He _ (go) to the gym every day."
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'went', callback_data='went')
        var2 = types.InlineKeyboardButton(text = 'going', callback_data='going')
        var3 = types.InlineKeyboardButton(text = 'goes', callback_data='goes')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q1, reply_markup = markup)
    if call.data in ['went','going']:
        q2="2.What is the correct form of 'to be'? I _ (be) a teacher."
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: goes")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'am', callback_data='am')
        var2 = types.InlineKeyboardButton(text = 'is', callback_data='is')
        var3 = types.InlineKeyboardButton(text = 'are', callback_data='are')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q2, reply_markup = markup)
    elif call.data =='goes':
        q2="2.What is the correct form of 'to be'? I _ (be) a teacher."
        bot.send_message(call.message.chat.id,"Rigth")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'am', callback_data='am')
        var2 = types.InlineKeyboardButton(text = 'is', callback_data='is')
        var3 = types.InlineKeyboardButton(text = 'are', callback_data='are')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q2, reply_markup = markup)
    if call.data in ['is','are']:
        q3="3.What is the correct form of 'can'? She _ (can) speak Spanish fluently."
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: am")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'could', callback_data='could')
        var2 = types.InlineKeyboardButton(text = 'can', callback_data='can')
        var3 = types.InlineKeyboardButton(text = 'will can', callback_data='will can')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q3, reply_markup = markup)
    elif call.data =='am':
        q3="3.What is the correct form of 'can'? She _ (can) speak Spanish fluently."
        bot.send_message(call.message.chat.id,"Rigth")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'could', callback_data='could')
        var2 = types.InlineKeyboardButton(text = 'can', callback_data='can')
        var3 = types.InlineKeyboardButton(text = 'will can', callback_data='will can')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q3, reply_markup = markup)
    if call.data in ['could','will can']:
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: can")
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")
    elif call.data =='can':
        bot.send_message(call.message.chat.id,"Rigth")
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")


@bot.callback_query_handler(func=lambda call: call.data in ["word_form1",'happiest','happily','happiness','dangerly','dangerously','dangerous','enjoiness','able','enjoy'])
def test_bwf(call):
    if call.data=="word_form1":
        q1="1.What is the noun from 'happy'?"
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'happiest', callback_data='happiest')
        var2 = types.InlineKeyboardButton(text = 'happiness', callback_data='happiness')
        var3 = types.InlineKeyboardButton(text = 'happily', callback_data='happily')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q1, reply_markup = markup)
    if call.data in ['happiest','happily']:
        q2="2.What is the adjective from 'danger'? "
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: happiness")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'dangerous', callback_data='dangerous')
        var2 = types.InlineKeyboardButton(text = 'dangerly', callback_data='dangerly')
        var3 = types.InlineKeyboardButton(text = 'dangerously', callback_data='dangerously')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q2, reply_markup = markup)
    elif call.data =='happiness':
        q2="2.What is the noun from 'dangerous'?"
        bot.send_message(call.message.chat.id,"Rigth")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'dangerous', callback_data='dangerous')
        var2 = types.InlineKeyboardButton(text = 'dangerly', callback_data='dangerly')
        var3 = types.InlineKeyboardButton(text = 'dangerously', callback_data='dangerously')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q2, reply_markup = markup)
    if call.data in ['dangerly','dangerously']:
        q3="3.What is the verb from 'enjoyable'? "
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: dangerous")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'enjoiness', callback_data='enjoyness')
        var2 = types.InlineKeyboardButton(text = 'enjoy', callback_data='enjoy')
        var3 = types.InlineKeyboardButton(text = 'able', callback_data='able')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q3, reply_markup = markup)
    elif call.data =='dangerous':
        q3="3.What is the verb from 'enjoyable'? "
        bot.send_message(call.message.chat.id,"Rigth")
        var1 = types.InlineKeyboardButton(text = 'enjoiness', callback_data='enjoyness')
        var2 = types.InlineKeyboardButton(text = 'enjoy', callback_data='enjoy')
        var3 = types.InlineKeyboardButton(text = 'able', callback_data='able')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q3, reply_markup = markup)
    if call.data in ['enjoiness','able']:
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: enjoy")
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")
    elif call.data =='enjoy':
        bot.send_message(call.message.chat.id,"Rigth")
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")
         
         
@bot.callback_query_handler(func=lambda call: call.data in ["vocabulary1",'banana','carrot','apple','elephan','cat','dog','cake','will can','ice cream'])
def test_bv(call):
    if call.data=="vocabulary1":
        q1="1.Translate into english the word 'яблоко'."
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'apple', callback_data='apple')
        var2 = types.InlineKeyboardButton(text = 'banana', callback_data='banana')
        var3 = types.InlineKeyboardButton(text = 'carrot', callback_data='carrot')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q1, reply_markup = markup)
    if call.data in ['banana','carrot']:
        q2="2.Translate into english the word 'собака'."
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: apple")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'dog', callback_data='dog')
        var2 = types.InlineKeyboardButton(text = 'elephan', callback_data='elephan')
        var3 = types.InlineKeyboardButton(text = 'cat', callback_data='cat')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q2, reply_markup = markup)
    elif call.data =='apple':
        q2="2.Translate into english the word 'собака'."
        bot.send_message(call.message.chat.id,"Rigth")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'dog', callback_data='dog')
        var2 = types.InlineKeyboardButton(text = 'elephan', callback_data='elephan')
        var3 = types.InlineKeyboardButton(text = 'cat', callback_data='cat')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q2, reply_markup = markup)
    if call.data in ['elephan','cat']:
        q3="3.Translate into english the word 'мороженое'."
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: dog")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'ice cream', callback_data='ice cream')
        var2 = types.InlineKeyboardButton(text = 'cake', callback_data='cake')
        var3 = types.InlineKeyboardButton(text = 'will can', callback_data='will can')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q3, reply_markup = markup)
    elif call.data =='dog':
        q3="3.Translate into english the word 'мороженое'."
        bot.send_message(call.message.chat.id,"Rigth")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'could', callback_data='could')
        var2 = types.InlineKeyboardButton(text = 'cake', callback_data='cake')
        var3 = types.InlineKeyboardButton(text = 'will can', callback_data='will can')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q3, reply_markup = markup)
    if call.data in ['cake','will can']:
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: ice cream")   
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")
    elif call.data =='ice cream':
        bot.send_message(call.message.chat.id,"Rigth")
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")

         
@bot.callback_query_handler(func=lambda call: call.data in ["grammar2",'have studied','am stugyng','study','am having','have','has','did','do','done'])
def test_mg(call):
    if call.data=="grammar2":          
        q1="1.What is the correct tense? I (study) English for 3 years."        
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'have studied', callback_data='have studied')
        var2 = types.InlineKeyboardButton(text = 'am stugyng', callback_data='am stugyng')
        var3 = types.InlineKeyboardButton(text = 'study', callback_data='study')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q1, reply_markup = markup)
    if call.data in ['am stugyng','study']:
        q2="2.What is the correct form of 'to have'? They _ (have) a big house."
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: have studied")         
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'am having', callback_data='am having')
        var2 = types.InlineKeyboardButton(text = 'have', callback_data='have')
        var3 = types.InlineKeyboardButton(text = 'has', callback_data='has')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q2, reply_markup = markup)
    elif call.data =='have studied':
        q2="2.What is the correct form of 'to have'? They _ (have) a big house."
        bot.send_message(call.message.chat.id,"Rigth")         
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'am having', callback_data='am having')
        var2 = types.InlineKeyboardButton(text = 'have', callback_data='have')
        var3 = types.InlineKeyboardButton(text = 'has', callback_data='has')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q2, reply_markup = markup)
    if call.data in ['am having','has']:
        q3="3.What is the correct form of 'to do'? We _ (do) our homework every night."
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: have")         
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'did', callback_data='did')
        var2 = types.InlineKeyboardButton(text = 'done', callback_data='done')
        var3 = types.InlineKeyboardButton(text = 'do', callback_data='do')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q3, reply_markup = markup)
    elif call.data =='have':
        q3="3.What is the correct form of 'to do'? We _ (do) our homework every night."
        bot.send_message(call.message.chat.id,"Rigth")       
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'did', callback_data='did')
        var2 = types.InlineKeyboardButton(text = 'done', callback_data='done')
        var3 = types.InlineKeyboardButton(text = 'do', callback_data='do')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q3, reply_markup = markup)
    if call.data in ['did','done']:
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: do")     
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")
    elif call.data =='do':
        bot.send_message(call.message.chat.id,"Rigth")
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")
         
         
@bot.callback_query_handler(func=lambda call: call.data in ["word_form2",'happiest','happily','happy','dangerly','dangerously','dangerous','enjoiness','able','enjoy'])
def test_mwf(call):
    if call.data=="word_form2":         
        q1="1.What is the noun from 'happy'?"         
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'happiest', callback_data='happiest')
        var2 = types.InlineKeyboardButton(text = 'happiness', callback_data='happiness')
        var3 = types.InlineKeyboardButton(text = 'happily', callback_data='happily')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q1, reply_markup = markup)
    if call.data in ['happiest','happily']:
        q2="2.What is the adjective from 'danger'? "
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: happiness")        
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'dangerous', callback_data='dangerous')
        var2 = types.InlineKeyboardButton(text = 'dangerly', callback_data='dangerly')
        var3 = types.InlineKeyboardButton(text = 'dangerously', callback_data='dangerously')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q2, reply_markup = markup)
    elif call.data =='happiness':
        q2="2.What is the noun from 'dangerous'?"
        bot.send_message(call.message.chat.id,"Rigth")         
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'dangerous', callback_data='dangerous')
        var2 = types.InlineKeyboardButton(text = 'dangerly', callback_data='dangerly')
        var3 = types.InlineKeyboardButton(text = 'dangerously', callback_data='dangerously')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q2, reply_markup = markup)
    if call.data in ['dangerly','dangerously']:
        q3="3.What is the verb from 'enjoyable'? "
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: dangerous")        
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'enjoiness', callback_data='enjoyness')
        var2 = types.InlineKeyboardButton(text = 'enjoy', callback_data='enjoy')
        var3 = types.InlineKeyboardButton(text = 'able', callback_data='able')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q3, reply_markup = markup)
    elif call.data =='dangerous':
        q3="3.What is the verb from 'enjoyable'? "
        bot.send_message(call.message.chat.id,"Rigth")        
        var1 = types.InlineKeyboardButton(text = 'enjoiness', callback_data='enjoyness')
        var2 = types.InlineKeyboardButton(text = 'enjoy', callback_data='enjoy')
        var3 = types.InlineKeyboardButton(text = 'able', callback_data='able')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q3, reply_markup = markup)
    if call.data in ['enjoiness','able']:
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: enjoy")
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")
    elif call.data =='enjoy':
        bot.send_message(call.message.chat.id,"Rigth")
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")
         
         
@bot.callback_query_handler(func=lambda call: call.data in ["vocabulary2",'banana','carrot','apple','elephan','cat','dog','candy','will can','ice cream'])
def test_mv(call):
    if call.data=="vocabulary2":
        q1="1.Translate into english the word 'яблоко'."
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'apple', callback_data='apple')
        var2 = types.InlineKeyboardButton(text = 'banana', callback_data='banana')
        var3 = types.InlineKeyboardButton(text = 'carrot', callback_data='carrot')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q1, reply_markup = markup)
    if call.data in ['banana','carrot']:
        q2="2.Translate into english the word 'собака'."
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: apple")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'dog', callback_data='dog')
        var2 = types.InlineKeyboardButton(text = 'elephan', callback_data='elephan')
        var3 = types.InlineKeyboardButton(text = 'cat', callback_data='cat')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q2, reply_markup = markup)
    elif call.data =='apple':
        q2="2.Translate into english the word 'собака'."
        bot.send_message(call.message.chat.id,"Rigth")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'dog', callback_data='dog')
        var2 = types.InlineKeyboardButton(text = 'elephan', callback_data='elephan')
        var3 = types.InlineKeyboardButton(text = 'cat', callback_data='cat')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q2, reply_markup = markup)
    if call.data in ['elephan','cat']:
        q3="3.Translate into english the word 'мороженое'."
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: dog")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'ice cream', callback_data='ice cream')
        var2 = types.InlineKeyboardButton(text = 'candy', callback_data='candy')
        var3 = types.InlineKeyboardButton(text = 'will can', callback_data='will can')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q3, reply_markup = markup)
    elif call.data =='dog':
        q3="3.Translate into english the word 'мороженое'."
        bot.send_message(call.message.chat.id,"Rigth")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'ice cream', callback_data='ice cream')
        var2 = types.InlineKeyboardButton(text = 'candy', callback_data='candy')
        var3 = types.InlineKeyboardButton(text = 'will can', callback_data='will can')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q3, reply_markup = markup)
    if call.data in ['candy','will can']:
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: ice cream")
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")
    elif call.data =='ice cream':
        bot.send_message(call.message.chat.id,"Rigth")
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")
         
         
@bot.callback_query_handler(func=lambda call: call.data in ["grammar3",'will eaten','was eaten','ate','will go','was going','goed','has','is having','having'])
def test_pg(call):
    if call.data=="grammar3":
        q1="1.What is the correct passive form 'to eat'? The pizza _ (eat) by John."
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'will eaten', callback_data='will eaten')
        var2 = types.InlineKeyboardButton(text = 'was eaten', callback_data='was eaten')
        var3 = types.InlineKeyboardButton(text = 'ate', callback_data='ate')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q1, reply_markup = markup)
    if call.data in ['will eaten','ate']:
        q2="2.What is the correct form of 'to go'? She _ (go) to the move next week."
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: was eaten")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'will go', callback_data='will go')
        var2 = types.InlineKeyboardButton(text = 'was going', callback_data='was going')
        var3 = types.InlineKeyboardButton(text = 'goed', callback_data='goed')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q2, reply_markup = markup)
    elif call.data =='was eaten':
        q2="2.What is the correct form of 'to go'? She _ (go) to the move next week."
        bot.send_message(call.message.chat.id,"Rigth")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'will go', callback_data='will go')
        var2 = types.InlineKeyboardButton(text = 'was going', callback_data='was going')
        var3 = types.InlineKeyboardButton(text = 'goed', callback_data='goed')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q2, reply_markup = markup)
    if call.data in ['was going','goed']:
        q3="3.What is the correct form of 'to have'? He _ (have) a lot of friends."
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: will go")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'has', callback_data='has')
        var2 = types.InlineKeyboardButton(text = 'is having', callback_data='is having')
        var3 = types.InlineKeyboardButton(text = 'having', callback_data='having')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q3, reply_markup = markup)
    elif call.data =='will go':
        q3="3.What is the correct form of 'to have'? He _ (have) a lot of friends."
        bot.send_message(call.message.chat.id,"Rigth")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'has', callback_data='has')
        var2 = types.InlineKeyboardButton(text = 'is having', callback_data='is having')
        var3 = types.InlineKeyboardButton(text = 'having', callback_data='having')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q3, reply_markup = markup)
    if call.data in ['is having','having']:
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: has")
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")
    elif call.data =='has':
        bot.send_message(call.message.chat.id,"Rigth")
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")
         
@bot.callback_query_handler(func=lambda call: call.data in ["word_form3",'imagine','image','imaginate','environment','viron','environt','huntere','hunt','enjoy'])
def test_pwf(call):
    if call.data=="word_form3":
        q1="1.What is the verb from 'imagination'? "
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'imagine', callback_data='imagine')
        var2 = types.InlineKeyboardButton(text = 'image', callback_data='image')
        var3 = types.InlineKeyboardButton(text = 'imaginate', callback_data='imaginate')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q1, reply_markup = markup)
    if call.data in ['image','imaginate']:
        q2="2.What is the noun from 'environmental'? "
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: imagine")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'environment', callback_data='environment')
        var2 = types.InlineKeyboardButton(text = 'viron', callback_data='viron')
        var3 = types.InlineKeyboardButton(text = 'environt', callback_data='environt')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q2, reply_markup = markup)
    elif call.data =='imagine':
        q2="2.What is the noun from 'environmental'?"
        bot.send_message(call.message.chat.id,"Rigth")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'environment', callback_data='environment')
        var2 = types.InlineKeyboardButton(text = 'viron', callback_data='viron')
        var3 = types.InlineKeyboardButton(text = 'environt', callback_data='environt')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q2, reply_markup = markup)
    if call.data in ['viron','environt']:
        q3="3.What is the verb from 'hunter'? "
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: environment")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'huntere', callback_data='huntere')
        var2 = types.InlineKeyboardButton(text = 'enjoy', callback_data='enjoy')
        var3 = types.InlineKeyboardButton(text = 'hunt', callback_data='hunt')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q3, reply_markup = markup)
    elif call.data =='environment':
        q3="3.What is the verb from 'hunter'? "
        bot.send_message(call.message.chat.id,"Rigth")
        var1 = types.InlineKeyboardButton(text = 'huntere', callback_data='huntere')
        var2 = types.InlineKeyboardButton(text = 'enjoy', callback_data='enjoy')
        var3 = types.InlineKeyboardButton(text = 'hunt', callback_data='hunt')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q3, reply_markup = markup)
    if call.data in ['huntere','aenjoy']:
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: enjoy")  
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")
    elif call.data =='hunt':
        bot.send_message(call.message.chat.id,"Rigth")
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")

@bot.callback_query_handler(func=lambda call: call.data in ["vocabulary3",'immediate','carrot','apple','elephan','cat','knowledgeable','againster','will can','oppponent'])
def test_pv(call):
    if call.data=="vocabulary3":
        q1="1.Translate into english the word 'немедленный'."
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'apple', callback_data='apple')
        var2 = types.InlineKeyboardButton(text = 'immediate', callback_data='immediate')
        var3 = types.InlineKeyboardButton(text = 'carrot', callback_data='carrot')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q1, reply_markup = markup)
    if call.data in ['apple','carrot']:
        q2="2.Translate into english the word 'знающий'."
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: immediate")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'knowledgeable', callback_data='knowledgeable')
        var2 = types.InlineKeyboardButton(text = 'elephan', callback_data='elephan')
        var3 = types.InlineKeyboardButton(text = 'cat', callback_data='cat')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q2, reply_markup = markup)
    elif call.data =='immediate':
        q2="2.Translate into english the word 'знающий'."
        bot.send_message(call.message.chat.id,"Rigth")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'dog', callback_data='dog')
        var2 = types.InlineKeyboardButton(text = 'elephan', callback_data='elephan')
        var3 = types.InlineKeyboardButton(text = 'cat', callback_data='cat')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q2, reply_markup = markup)
    if call.data in ['elephan','cat']:
        q3="3.Translate into english the word 'противник'."
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: opponent")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'opponent', callback_data='opponent')
        var2 = types.InlineKeyboardButton(text = 'againster', callback_data='againster')
        var3 = types.InlineKeyboardButton(text = 'will can', callback_data='will can')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id, q3, reply_markup = markup)
    elif call.data =='knowledgeable':
        q3="3.Translate into english the word 'противник'."
        bot.send_message(call.message.chat.id,"Rigth")
        markup = types.InlineKeyboardMarkup()
        var1 = types.InlineKeyboardButton(text = 'opponent', callback_data='opponent')
        var2 = types.InlineKeyboardButton(text = 'againster', callback_data='againster')
        var3 = types.InlineKeyboardButton(text = 'will can', callback_data='will can')
        markup.add(var1, var2,var3)
        bot.send_message(call.message.chat.id,  q3, reply_markup = markup)
    if call.data in ['againster','will can']:
        bot.send_message(call.message.chat.id,"Wrong\nThe right answer is: opponent")  
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")
    elif call.data =='opponent':
        bot.send_message(call.message.chat.id,"Rigth")
        bot.send_message(call.message.chat.id,  "That's all\nНапиши 'начать новый тест' ")

bot.polling(none_stop=True)